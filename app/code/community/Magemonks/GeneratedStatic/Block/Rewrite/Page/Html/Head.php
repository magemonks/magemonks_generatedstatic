<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * This is the modified Mage_Page_Block_Html_Head to allow generated js / generated css
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_GeneratedStatic_Block_Rewrite_Page_Html_Head extends Mage_Page_Block_Html_Head
{
    /**
     * Add HEAD Item
     *
     * @param string $type
     * @param string $name
     * @param string $params
     * @param string $if
     * @param string $cond
     * @return Mage_Page_Block_Html_Head
     */
    public function addItem($type, $name, $params=null, $if=null, $cond=null)
    {
        if ($type === 'generated_css' && empty($params)) {
            $params = 'media="all"';
        }
        return parent::addItem($type, $name, $params, $if, $cond);
    }


    public function getItems()
    {
        return $this->_data['items'];
    }


    /**
     * Get HEAD HTML with CSS/JS/RSS definitions
     *
     * @return string
     */
    public function getCssJsHtml()
    {
        // separate items by types
        $lines  = array();
        foreach ($this->_data['items'] as $item) {
            if (!is_null($item['cond']) && !$this->getData($item['cond']) || !isset($item['name'])) {
                continue;
            }
            $if     = !empty($item['if']) ? $item['if'] : '';
            $params = !empty($item['params']) ? $item['params'] : '';
            switch ($item['type']) {
                case 'js':        // js/*.js
                case 'skin_js':   // skin/*/*.js
                case 'js_css':    // js/*.css
                case 'skin_css':  // skin/*/*.css
                case 'generated_css':
                case 'generated_js':
                    $lines[$if][$item['type']][$params][$item['name']] = $item['name'];
                    break;
                default:
                    $this->_separateOtherHtmlHeadElements($lines, $if, $item['type'], $params, $item['name'], $item);
                    break;
            }
        }

        // prepare HTML
        $shouldMergeJs = Mage::getStoreConfigFlag('dev/js/merge_files');
        $shouldMergeCss = Mage::getStoreConfigFlag('dev/css/merge_css_files');
        $html   = '';
        foreach ($lines as $if => $items) {
            if (empty($items)) {
                continue;
            }
            if (!empty($if)) {
                $html .= '<!--[if '.$if.']>'."\n";
            }

            // static and skin css
            $html .= $this->_prepareStaticAndSkinElements(
                '<link rel="stylesheet" type="text/css" href="%s"%s />' . "\n",
                empty($items['js_css']) ? array() : $items['js_css'],
                empty($items['skin_css']) ? array() : $items['skin_css'],
                $shouldMergeCss ? array(Mage::getDesign(), 'getMergedCssUrl') : null
            );

            // static and skin javascripts
            $html .= $this->_prepareStaticAndSkinElements(
                '<script type="text/javascript" src="%s"%s></script>' . "\n",
                empty($items['js']) ? array() : $items['js'],
                empty($items['skin_js']) ? array() : $items['skin_js'],
                $shouldMergeJs ? array(Mage::getDesign(), 'getMergedJsUrl') : null
            );

            // generated css
            $html .= $this->_prepareGeneratedElements(
                '<link rel="stylesheet" type="text/css" href="%s"%s />' . "\n",
                empty($items['generated_css']) ? array() : $items['generated_css'],
                $if,
                'css'
            );

            // generated js
            $html .= $this->_prepareGeneratedElements(
                '<script type="text/javascript" src="%s"%s></script>' . "\n",
                empty($items['generated_js']) ? array() : $items['generated_js'],
                $if,
                'js'
            );

            // other stuff
            if (!empty($items['other'])) {
                $html .= $this->_prepareOtherHtmlHeadElements($items['other']) . "\n";
            }

            if (!empty($if)) {
                $html .= '<![endif]-->'."\n";
            }
        }
        return $html;
    }

    /**
     * Prepare generated css / js items
     *
     * @param string $format
     * @param array $generatedItems
     * @param string $type
     * @return string
     */
    protected function &_prepareGeneratedElements($format, array $generatedItems, $if = null, $type)
    {
        /**
         * @var $helper Magemonks_Core_Helper_Static
         */
        $helper = Mage::helper('generatedStatic/data');

        $items = array();

        foreach ($generatedItems as $params => $rows) {
            foreach ($rows as $name) {
                $items[$params][] = $helper->getUrl($type, $name, $params, $if);
            }
        }

        $html = '';
        foreach ($items as $params => $rows) {
            // render elements
            $params = trim($params);
            $params = $params ? ' ' . $params : '';
            foreach ($rows as $src) {
                $html .= sprintf($format, $src, $params);
            }
        }
        return $html;
    }
}
