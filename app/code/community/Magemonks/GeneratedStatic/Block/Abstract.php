<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * This is the modified Mage_Page_Block_Html_Head to allow generated js / generated css
 *
 * @category    Magemonks
 * @package     Magemonks_GeneratedStatic
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
abstract class Magemonks_GeneratedStatic_Block_Abstract extends Mage_Core_Block_Template
{
    public function _construct(){
        if($this->getHeadType() == 'css'){
            $this->setData('template', $this->getData('css_template'));
        }
        elseif($this->getHeadType() == 'js'){
            $this->setData('template', $this->getData('js_template'));
        }
        parent::_construct();
    }

    /**
     * Get cache key informative items
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        $array =  array(
            'STATIC_',
            Mage::app()->getStore()->getCode(),
            $this->getTemplate(),
            $this->getTemplateFile(),
            (string) md5($this->getType()),
            (string) md5($this->getParams()),
            (string) md5($this->getIf()),
        );
        return $array;
    }

    /**
     * Get the head type (normally js or css)
     *
     * @return string|null
     */
    public function getHeadType()
    {
        return $this->getData('head_type');
    }

    /**
     * Get the paramaters for wich the file was included
     *
     * @return string|null
     */
    public function getHeadParams()
    {
        return $this->getData('head_params');
    }

    /**
     * Gets the if statement for wich the file was included
     *
     * @return string|null
     */
    public function getHeadIf()
    {
        return $this->getData('head_if');
    }
}