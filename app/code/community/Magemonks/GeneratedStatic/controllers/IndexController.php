<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_Core
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_GeneratedStatic_IndexController extends Mage_Core_Controller_Front_Action{

    /**
     * Index action
     * @return Magemonks_GeneratedStatic_IndexController
     */
    public function indexAction()
    {
        /**
         * @var $helper Magemonks_GeneratedStatic_Helper_Data
         */
        $helper = Mage::helper('generatedStatic/data');

        $path = $this->getRequest()->getParam('path', null);
        $type = $this->getRequest()->getParam('type', null);

        if(empty($path) || empty($type)){
            $this->getResponse()->setHttpResponseCode(404);
            $this->getResponse()->setBody('');
            return;
        }

        $info = $helper->decryptPath($path);
        if(is_null($info) || !isset($info['name']) || empty($info['name'])){
            $this->getResponse()->setHttpResponseCode(404);
            $this->getResponse()->setBody('');
            return;
        }

        $block = $this->getLayout()->createBlock($info['name'], 'static', array(
            'head_type' => $type,
            'head_params' => $info['params'],
            'head_if' => $info['if']
        ));
        $this->getResponse()->setHeader('Content-type', $this->_getContentType($type).';charset=UTF-8');
        $this->getResponse()->setBody($block->toHtml());

        try{

        }
        catch(Exception $e){
            $this->getResponse()->setHttpResponseCode(404);
            $this->getResponse()->setBody('');
            return;
        }
        return $this;
    }

    /**
     * Get the content type
     *
     * @param $type
     * @return string
     */
    protected function _getContentType($type)
    {
        switch($type){
            case 'css':
                return 'text/css';
            case 'js':
                return 'application/x-javascript';
            default:
                return 'text/plain';
        }
    }
}