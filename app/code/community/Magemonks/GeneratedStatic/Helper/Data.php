<?php
/**
 * MAGEMONKS
 *
 * LICENSE: This source file is subject to the EULA that is bundled with
 * this package in the file LICENSE.txt. It is also available through the
 * world-wide-web at the following URI: http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 *
 * @category    Magemonks
 * @package     Magemonks_GeneratedStatic
 * @author      Magemonks <info@magemonks.com>
 * @copyright   2012 Magemonks (http://www.magemonks.com)
 * @license     http://www.magemonks.com/MAGEMONKS-LICENSE-COMMUNITY.txt
 * @version     Release: @package_version@
 */
class Magemonks_GeneratedStatic_Helper_Data extends Mage_Core_Helper_Abstract{

    CONST STATIC_DIR = 'generatedStatic';
    CONST SECRET = 'magemonks';
    const XML_NODE_CRYPT_KEY = 'global/crypt/key';

    public function getUrl($type, $name, $params, $if)
    {
        $isSecure = Mage::app()->getRequest()->isSecure();
        $baseMediaUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_DIRECT_LINK, $isSecure);

        $path = serialize(array('name' => $name, 'params' => $params, 'if' => $if));
        return $baseMediaUrl . self::STATIC_DIR . '/' . $this->encryptPath($path) . '.' . $type;
    }

    public function encryptPath($path)
    {
        return $this->_urlBase64Encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($this->_getSecret()), $path, MCRYPT_MODE_CBC, md5(md5($this->_getSecret()))));
    }

    public function decryptPath($path)
    {
        $path = $this->_urlBase64Decode($path);
        try{
            return unserialize(rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($this->_getSecret()), $path, MCRYPT_MODE_CBC, md5(md5($this->_getSecret()))), "\0"));
        }
        catch(Exception $e){
            return null;
        }
    }

    protected function _urlBase64Encode($input)
    {
        $input = base64_encode($input);
        return str_replace(array('+', '/'), array('_', '-'), $input);
    }

    protected function _urlBase64Decode($input)
    {
        $input = str_replace(array('_', '-'), array('+', '/'), $input);
        return base64_decode($input);
    }

    protected function _getSecret()
    {
        return self::SECRET.substr((string) Mage::getConfig()->getNode(self::XML_NODE_CRYPT_KEY), 3);
    }
}